filetype off                
set nocompatible
set encoding=utf-8
set number
set noswapfile
set colorcolumn=79,100
set scrolloff=5
set laststatus=2
" because with the tree I can get lazy :-P
set mouse=a

syntax enable
syntax on

au BufNewFile,BufRead *.py set tabstop=4
au BufNewFile,BufRead *.py set softtabstop=4
au BufNewFile,BufRead *.py set shiftwidth=4
au BufNewFile,BufRead *.py set textwidth=79
au BufNewFile,BufRead *.py set expandtab
au BufNewFile,BufRead *.py set autoindent

set autoindent
set fileformat=unix

filetype plugin indent on

" vim-plug
call plug#begin()

" Some themes I like, sometimes I might switch depending on light and source
Plug 'jnurmine/Zenburn'
Plug 'GlennLeo/cobalt2'
Plug 'altercation/vim-colors-solarized'
let g:gruvbox_contrast_dark='hard'
let g:gruvbox_contrast_light='medium'
Plug 'morhetz/gruvbox'
Plug 'NLKNguyen/papercolor-theme'


" NERD Tree for tree file
Plug 'preservim/nerdtree'
" NERD Tree keymap and settings
nmap <C-n> :NERDTreeToggle<CR>
let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree

" NERD Commenter
Plug 'preservim/nerdcommenter'
" NERD Commenter key mappings
nmap <C-_> <Plug>NERDCommenterToggle
vmap <C-S-7> <Plug>NERDCommenterToggle<CR>gv

" Better PEP 8 indentation for python
Plug 'vim-scripts/indentpython.vim'

" Autocomplete (needs additional install in its directory)
Plug 'Valloric/YouCompleteMe'

" Syntax checking (for python) - deprecated consider alternatinve..."
" syntastic settings
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_python_checkers = ['pylint']
Plug 'vim-syntastic/syntastic'

Plug 'vim-python/python-syntax/'

" Powerline plugin
Plug 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}

call plug#end()

" To get true color colourscheme in terminal
if(has("termguicolors"))
   set termguicolors
endif

" Choice of default colorscheme (must be installed)
colorscheme gruvbox

if has('gui_running')
  set lines=35 columns=90 linespace=0
  set guifont=Hack\ Regular\ 16
  set bg=dark
  map  <silent>  <S-Insert>  "+p
  imap <silent>  <S-Insert>  <Esc>"+pa
endif

let g:SimpylFold_docstring_preview=1

let g:ycm_autoclose_preview_window_after_completion=1

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

augroup vimrc_python
	au!
 	let python_highlight_all=1
	autocmd FileType python nnoremap <buffer> <F5> :w \| exec '!clear; python' shellescape(@%, 1)<cr>
augroup END


